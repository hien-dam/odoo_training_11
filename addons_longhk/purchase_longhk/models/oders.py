from odoo import models, fields, api, _

from datetime import datetime, date
from odoo.exceptions import ValidationError

states = [('do', 'Dự thảo'), ('doing', 'Chờ Duyệt'), ('done', 'Đã duyệt'), ('success', 'Hoàn thành'), ('reject', 'Hủy')]


class orders(models.Model):
    _name = 'purchasedemo.orders'
    _description = 'orders'
    _rec_name = 'reference_no'

    reference_no = fields.Char(string='Order Reference', required=True, readonly=True, default=_('New'))
    approved_by = fields.Char('Người phê duyệt')
    # requests_by = fields.Char('Người yêu cầu', required=True, default=lambda self: self.env.user.name)
    user_id = fields.Many2one('res.users', default=lambda self: self.env.user)
    department_id = fields.Many2one('hr.department', required=True)
    # department_id = fields.Char()
    cost_total = fields.Float('Cost Total', compute='_cost_total', store=True)
    creation_date = fields.Date('Creation Date')
    due_date = fields.Date('Due date')
    approved_date = fields.Date('Approved Date')
    # company = fields.Char('Công Ty', default='My company')
    company_id = fields.Many2one('res.company', readonly=True, default=lambda self: self.env.user.company_id)
    reject_reason = fields.Char('Lý do từ chối duyệt')
    status = fields.Selection(states, default=states[0][0])

    order_line_ids = fields.One2many('purchasedemo.ordersline', 'order_id', string='Order Lines',
                                     auto_join=True)


    @api.constrains('order_line_ids')
    def _check_product_exits(self):
        for product in self:
            exist_product_list = []
            for line in product.order_line_ids:
                if line.product_id in exist_product_list:
                    raise ValidationError("Không được trùng sản phẩm")
                exist_product_list.append(line.product_id)

    @api.model
    def create(self, vals):
        if vals.get('reference_no', _('New')) == _('New'):
            sequence = self.env['ir.sequence'].next_by_code('purchase.ticket')
            if sequence:
                vals['reference_no'] = sequence
        res = super(orders, self).create(vals)
        return res

    @api.onchange('due_date')
    def _onchange_approved_date(self):
        for order in self:
            for date in order.order_line_ids:
                date.approved_date = order.due_date



    def doing_confirm(self):
        for order in self:
            order.status = states[1][0]

    def done_confirm(self):
        for order in self:
            order.approved_by = self.env.user.name
            order.approved_date = date.today()
            order.status = states[2][0]

    def reject_confirm(self):
        for order in self:
            order.status = states[4][0]
            order.approved_by = self.env.user.name
        return {
            'name': "Your String",
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'purchasedemo.orderupdate.wizard',
            'view_id': self.env.ref('purchase_longhk.view_orders_update').id,
            'target': 'new',
            'context': {

                'default_order_id': self.id,
            }
        }

    def do_confirm(self):
        for order in self:
            order.reject_reason = ''
            order.status = states[0][0]
            order.approved_by = ''

    @api.depends('order_line_ids.cost')
    def _cost_total(self):
        for cost in self:
            cost.cost_total = 0
            cots_total = 0
            for i in cost.order_line_ids:
                cots_total += i.cost
            cost.cost_total += cots_total
