# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError
from datetime import datetime, date

class products(models.Model):
    _name = 'purchasedemo.products'
    _description = 'products'
    _rec_name = 'product'

    product = fields.Char('Sản phẩm')
    unit = fields.Many2one('uom.uom')
    unit_price = fields.Float('Đơn giá dự kiến')
    order_id = fields.Many2one('purchasedemo.orders')
    order_line_ids = fields.One2many('purchasedemo.ordersline', 'order_id', string='Order Lines',
                                 auto_join=True)

    @api.constrains('order_line_ids')
    def _check_product_exits(self):
        for product in self:
            exist_product_list = []
            for line in product.order_line_ids:
                if line.id in exist_product_list:
                    raise ValidationError("product have exists")
                exist_product_list.append(line.id)


