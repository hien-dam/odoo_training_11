# -*- coding: utf-8 -*-

from odoo import models, fields, api, _

from datetime import datetime, date
from odoo.exceptions import ValidationError

class orderline(models.Model):
    _name = 'purchasedemo.ordersline'
    _description = 'orders_line'
    order_id = fields.Many2one('purchasedemo.orders', string='Order Reference', required=True, ondelete='cascade',
                               index=True,
                               copy=False)
    amount = fields.Integer('Số lượng yêu cầu', default=0)
    cost = fields.Float('Chi phí dự kiến', compute="_cost_total", store=True)
    approved_date = fields.Date('Ngày Cần Cấp', default=date.today())
    description = fields.Char('Ghi Chú')
    unit = fields.Char('Đơn vị tính', related='product_id.unit.name')
    unit_price = fields.Float('Đơn giá dự kiến', related='product_id.unit_price')
    product_id = fields.Many2one('purchasedemo.products', string='Product', change_default=True,
                                 ondelete='restrict', )



    @api.constrains('amount')
    def _check_product_exits(self):
        for product in self:
            if product.amount <1:
                raise ValidationError("Số lượng phải lớn hơn 0")


    @api.depends('product_id.unit_price', 'amount')
    def _cost_total(self):
        for price in self:
            price.cost = price.product_id.unit_price * price.amount

    @api.depends('product_id')
    def _create_unit(self):
        for unit in self:
            unit.unit = unit.product_id.unit

    @api.depends('product_id')
    def _create_unit_price(self):
        for unit_price in self:
            unit_price.unit_price = unit_price.product_id.unit_price


