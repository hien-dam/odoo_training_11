from odoo import api, fields, models, tools
from datetime import datetime



class OrderUpdateWizard(models.TransientModel):
    _name = 'purchasedemo.orderupdate.wizard'
    _description = 'update wizard'

    date = fields.Date('Ngày')
    description = fields.Text('Lý do')
    order_id = fields.Many2one('purchasedemo.orders', string='Order Reference', ondelete='cascade', index=True,)

    def action_update_orders(self):
        des=str(self.date.strftime('%Y%m%d')) + ':' + str(self.description)
        self.order_id.write({'reject_reason': str(self.order_id.reject_reason) + str(des)})