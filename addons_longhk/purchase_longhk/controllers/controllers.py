from odoo import http

from odoo.http import request

import json


class ControllerDemo(http.Controller):

    @http.route('/get_id/<int:id>', auth='public', type='http', website='True')
    def controller_check(self, id,**post):
        data = request.env['purchasedemo.orders'].sudo().search([('id', '=', int(id))], limit=1)
        response = {
            "status": "done",
            "content": {
                "reference_no": data.reference_no,


            }
        }
        return request.render(response)
