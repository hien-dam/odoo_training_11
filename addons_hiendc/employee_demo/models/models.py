# -*- coding: utf-8 -*-

from odoo import models, fields, api


class EmployeeDemo(models.Model):
    _name = 'employee.demo'
    _description = 'Employee Demo'

    name = fields.Char()
    value = fields.Integer()
    value2 = fields.Float(compute="_value_pc", store=False)
    description = fields.Text()
    age = fields.Integer(string='Age')
    dob = fields.Date(string='Dob')

    @api.depends('value')
    def _value_pc(self):
        for record in self:
            record.value2 = float(record.value) / 100

    def action_demo(self):
        print(self.id)

    @api.model
    def create(self, values):
        name = values.get('name').upper()
        values['name'] = name
        return super().create(values)
