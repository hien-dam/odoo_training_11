# -*- coding: utf-8 -*-
{
    'name': "purchase",

    'summary': """
        Purchase Request""",

    'description': """
        Yêu cầu mua hàng""",

    'author': "Odoo",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/15.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Sales/Purchase Request',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'hr', 'product', 'mail'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'security/purchase_request_security.xml',
        'views/purchase_request.xml',
        'data/ir_sequence_data.xml',
        'data/mail_template_data.xml',
        'wizard/reject_reason.xml',
        'wizard/purchase_request_line_import_view.xml',
    ],
    'images': [
        'static/description/icon.png',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
    'license': "LGPL-3",
    'application': True,
}
