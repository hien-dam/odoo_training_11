from odoo import fields, models


class PurchaseRejectReason(models.TransientModel):
    _name = 'purchase.reject.reason'
    _description = 'Purchase reject reason'

    purchase_request_id = fields.Many2one('purchase.request', required=True, ondelete='cascade')
    date = fields.Date(default=fields.Date.context_today)
    description = fields.Text()

    def action_reject_reason(self):
        self.purchase_request_id.write({
            'reject_reason': str(self.date) + ":" + self.description})
