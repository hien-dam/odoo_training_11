from odoo import models, fields, _
import openpyxl
import base64
from io import BytesIO
from odoo.exceptions import UserError


class ImportPurchaseRequestLine(models.TransientModel):
    _name = "purchase.request.line.import"

    file = fields.Binary(string="File", required=True)

    def purchase_request_line_import(self):
        try:
            wb = openpyxl.load_workbook(
                filename=BytesIO(base64.b64decode(self.file)), read_only=True
            )
            ws = wb.active
            for record in ws.iter_rows(min_row=2, max_row=None, min_col=None, max_col=None, values_only=True):
                product_id = self.env['product.product'].search([('default_code', '=', record[0])])
                if product_id:
                    value = {
                        'product_id': product_id.id,
                        'request_quantity': record[1],
                        'estimated_unit_price': record[2],
                        'estimated_subtotal': record[3],
                        'due_date': record[4],
                    }
                    self.env['purchase.request.line'].create(value)
        except:
            raise UserError(_('Please insert a valid file'))
