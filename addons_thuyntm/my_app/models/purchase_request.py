from odoo import models, fields, api, _
from datetime import datetime, date
from odoo.exceptions import ValidationError, UserError


class PurchaseRequest(models.Model):
    _name = "purchase.request"
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = "Purchase Request"

    name = fields.Text(
        'Name',
        required=True,
        readonly=True,
        default=_('New'))
    requested_by_id = fields.Many2one(
        'res.users',
        string='Requested by',
        required=True, readonly=False, tracking=True,
        default=lambda self: self.env.uid)
    department_id = fields.Many2one(
        'hr.department', string='Department', readonly=False, tracking=True,
        default=lambda self: self.env.user.department_id)
    cost_total = fields.Float('Cost total', compute='_compute_cost_total')
    creation_date = fields.Date('Creation date', readonly=False, tracking=True, default=fields.Date.context_today)
    due_date = fields.Date('Due date', tracking=True)
    approved_date = fields.Date('Approved date', readonly=True, tracking=True)
    company_id = fields.Many2one('res.company', string='Company', readonly=True,
                                 default=lambda self: self.env.user.company_id)
    reject_reason = fields.Char('Reject reason')

    purchase_request_line_ids = fields.One2many('purchase.request.line', 'purchase_request_id',
                                                string='Purchase Request Lines')
    state = fields.Selection([
        ('draft', 'Draft'),
        ('waiting approval', 'Waiting Approval'),
        ('to approve', 'To Approve'),
        ('done', 'Done'),
        ('reject', 'Reject'),
        ('cancel', 'Cancelled')
    ], string='Status', readonly=True, copy=False, default='draft')

    @api.model
    def create(self, vals):
        if vals.get('name', _('New')) == _('New'):
            sequence = self.env['ir.sequence'].next_by_code('purchase.request.name')
            if sequence:
                vals['name'] = sequence
        res = super(PurchaseRequest, self).create(vals)
        return res

    @api.onchange('due_date')
    def _onchange_due_date(self):
        self.purchase_request_line_ids.write({'due_date': self.due_date})

    @api.constrains('purchase_request_line_ids')
    def _check_product(self):
        for product_id in self:
            product_list = []
            for line in product_id.purchase_request_line_ids:
                if line.product_id in product_list:
                    raise ValidationError("The product is duplicate")
                product_list.append(line.product_id)

    @api.depends('purchase_request_line_ids')
    def _compute_cost_total(self):
        for record in self:
            cost_total = 0
            for product in record.purchase_request_line_ids:
                cost_total += product.estimated_subtotal
            record.cost_total = cost_total

    def action_request_approval(self):
        self.write({'state': 'waiting approval'})

    def button_approve(self):
        self.write({
            'state': 'to approve',
            'approved_date': date.today()
        })

    def button_reject(self):
        self.write({
            'state': 'reject',
            'approved_date': date.today()
        })

        return {
            'name': "Reject",
            'view_mode': 'form',
            'res_model': 'purchase.reject.reason',
            'view_id': self.env.ref('my_app.reject_reason_view_form').id,
            'type': 'ir.actions.act_window',
            'context': {'default_purchase_request_id': self.id},
            'target': 'new'
        }

    def button_cancel(self):
        self.write({'state': 'cancel'})

    def back_to_draft(self):
        self.write({'state': 'draft'})

    def button_done(self):
        self.write({'state': 'done'})

    def button_import(self):
        return {
            'name': "Import",
            'view_mode': 'form',
            'res_model': 'purchase.request.line.import',
            'view_id': self.env.ref('my_app.purchase_request_line_import_view_from').id,
            'type': 'ir.actions.act_window',
            'context': {'default_purchase_request_id': self.id},
            'target': 'new'
        }

    def action_send_email(self):
        self.ensure_one()
        try:
            template_id = self.env.ref('my_app.email_template_name').id
        except ValueError:
            template_id = False
        try:
            compose_form_id = self.env.ref('mail.email_compose_message_wizard_form').id
        except ValueError:
            compose_form_id = False
        ctx = {
            'default_model': 'purchase.request',
            'default_res_id': self.ids[0],
            'default_use_template': bool(template_id),
            'default_template_id': template_id,
            'default_composition_mode': 'comment',
        }
        return {
            'name': _('Compose Email'),
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'views': [(compose_form_id, 'form')],
            'view_id': compose_form_id,
            'target': 'new',
            'context': ctx,
        }


class PurchaseRequestLine(models.Model):
    _name = "purchase.request.line"
    _description = "Purchase Request Line"

    purchase_request_id = fields.Many2one(
        'purchase.request',
        string='Purchase Request')
    product_id = fields.Many2one(
        'product.product',
        string='Product',
        required=True)
    product_uom = fields.Many2one(
        'uom.uom',
        string='Unit of Measure')
    request_quantity = fields.Integer(
        'Request quantity',
        required=True)
    estimated_unit_price = fields.Float(
        'Estimated unit price',
        related="product_id.list_price")
    estimated_subtotal = fields.Float('Estimated Subtotal', compute='_compute_estimated_subtotal')
    due_date = fields.Date('Due date', required=True, readonly=False)
    description = fields.Char('Description')
    delivered_quantity = fields.Integer('Delivered quantity')

    @api.onchange('due_date')
    def _onchange_due_date(self):
        for date in self:
            date.due_date = date.purchase_request_id.due_date

    @api.constrains('request_quantity')
    def _check_request_quantity(self):
        for quantity in self:
            if quantity.request_quantity < 1:
                raise ValidationError("Number of requests must be greater than 0!")

    @api.depends('estimated_unit_price', 'request_quantity')
    def _compute_estimated_subtotal(self):
        for record in self:
            record.estimated_subtotal = record.estimated_unit_price * record.request_quantity
