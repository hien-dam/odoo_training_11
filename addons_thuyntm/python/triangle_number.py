number = int(input("Nhap chiều dài của tam giác: "))
for i in range(1, number + 1):
    for j in range(1, i + 1):
        print(i, end=' ')
    print('\n')

# Tam giác rỗng * bên trái
for i in range(1,number+1):
    for j in range(1,i+1):
        if (j==1 or j==i) and i!=number:
            print("*", end="")
        elif (j!=1 and j!=i) and i!=number:
            print(end=" ")
        elif i==number:
            print("*", end="")
    print()

# Tam giác rỗng * bên phải
for i in range(1,number+1):
    for k in range(i,number+1):
        print(end=" ")
    for j in range(1,i+1):
        if (j==1 or j==i) and i!=number:
            print("*", end="")
        elif (j!=1 and j!=i) and i!=number:
            print(end=" ")
        elif i==number:
            print("*", end="")
    print()

# Tam giác rỗng số
for i in range(1,number+1):
    for j in range(1,i+1):
        if (j==1 or j==i) and i!=number:
            print(i, end="")
        elif (j!=1 and j!=i) and i!=number:
            print(end=" ")
        elif i==number:
            print(i, end="")
    print()

