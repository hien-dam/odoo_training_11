# Tinh e
s=1.0
ts=1.0
ms=1.0
n=20
x=1.0
for i in range(1,n+1):
    ts*=x
    ms*=i
    s+=(ts/ms)
print(s)

# Tinh cos
s = 1.0
ts = 1.0
ms = 1.0
n = 20
x = 1
for i in range(1,n+1):
    ts*=x
    ms*=i
    if n%2 == 0:
        if (i/2)%2 == 0:
            s += (ts/ms)
        else:
            s += -(ts/ms)
print(s)

#Tinh sin
ts = 1.0
ms = 1.0
n = 20
x = 4
s = 0

for i in range(1, n + 1):
    ts *= x
    ms *= i
    if i % 2 == 1:

        if (i // 2) % 2 == 0:
            s += (ts / ms)
        else:
            s -= (ts / ms)
print(s)