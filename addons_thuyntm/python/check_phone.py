phone = str(input("Nhap vao so dien thoai: "))
try:
  if not phone.isnumeric():
    raise ValueError("NO")
  if not len(phone) == 10:
    raise ValueError("Phone must be 10 characters")
  if not phone[0] == '0':
    raise ValueError("Phone number must start 0")
except ValueError as e:
  raise e
