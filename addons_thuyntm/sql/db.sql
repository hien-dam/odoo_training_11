CREATE TABLE items (
	itemID SERIAL PRIMARY KEY,
	name varchar(255) not null,
	price float not null
);
CREATE TABLE itemVariants (
	variantID SERIAL PRIMARY KEY,
	detail varchar(200) not null,
	color varchar(50) not null,
	size_s varchar(30) not null,
	itemID integer REFERENCES items
);
CREATE TABLE categories (
	catID SERIAL PRIMARY KEY,
	name varchar(255) not null
);

CREATE TABLE catid_ir_itemid_rel (
	catID integer REFERENCES categories,
	itemID integer REFERENCES items
);

select * from items
insert into items(name,price) values ('Keo',10.0), ('COca',20.00);


create table DEPARTMENT (
    DEPT_ID bigint not null,
    DEPT_NAME varchar(255) not null,
    DEPT_NO varchar(20) not null,
    LOCATION varchar(255),
    primary key (DEPT_ID),
    unique (DEPT_NO)
);
 
create table EMPLOYEE (
    EMP_ID bigint not null,
    EMP_NAME varchar(50) not null,
    EMP_NO varchar(20) not null,
    HIRE_DATE date not null,
    IMAGE bytea,
    JOB varchar(30) not null,
    SALARY float not null,
    DEPT_ID bigint not null,
    MNG_ID bigint,
    primary key (EMP_ID),
    unique (EMP_NO)
);
 
create table SALARY_GRADE (
    GRADE int not null,
    HIGH_SALARY float not null,
    LOW_SALARY float not null,
    primary key (GRADE)
);
 
create table TIMEKEEPER (
    Timekeeper_Id varchar(36) not null,
    Date_Time timestamp not null,
    In_Out char(1) not null,
    EMP_ID bigint not null,
    primary key (Timekeeper_Id)
);
 
alter table EMPLOYEE
    add constraint FK75C8D6AE269A3C9
    foreign key (DEPT_ID)
    references DEPARTMENT;
 
alter table EMPLOYEE
    add constraint FK75C8D6AE6106A42
    foreign key (EMP_ID)
    references EMPLOYEE;
 
alter table EMPLOYEE
    add constraint FK75C8D6AE13C12F64
    foreign key (MNG_ID)
    references EMPLOYEE;
 
alter table TIMEKEEPER
    add constraint FK744D9BFF6106A42
    foreign key (EMP_ID)
    references EMPLOYEE;

insert into Department (DEPT_ID, DEPT_NAME, DEPT_NO, LOCATION)
values (10, 'ACCOUNTING', 'D10', 'NEW YORK');
 
insert into Department (DEPT_ID, DEPT_NAME, DEPT_NO, LOCATION)
values (20, 'RESEARCH', 'D20', 'DALLAS');
 
insert into Department (DEPT_ID, DEPT_NAME, DEPT_NO, LOCATION)
values (30, 'SALES', 'D30', 'CHICAGO');
 
insert into Department (DEPT_ID, DEPT_NAME, DEPT_NO, LOCATION)
values (40, 'OPERATIONS', 'D40', 'BOSTON');
 
-------------------------------------------------------------------------------------------------
 
insert into Employee (EMP_ID, EMP_NAME, EMP_NO, HIRE_DATE, JOB, SALARY, DEPT_ID, MNG_ID)
values (7839, 'KING', 'E7839', to_date('17-11-1981', 'dd-mm-yyyy'), 'PRESIDENT', 5000, 10, null);
 
insert into Employee (EMP_ID, EMP_NAME, EMP_NO, HIRE_DATE, JOB, SALARY, DEPT_ID, MNG_ID)
values (7566, 'JONES', 'E7566', to_date('02-04-1981', 'dd-mm-yyyy'), 'MANAGER', 2975, 20, 7839);
 
insert into Employee (EMP_ID, EMP_NAME, EMP_NO, HIRE_DATE, JOB, SALARY, DEPT_ID, MNG_ID)
values (7902, 'FORD', 'E7902', to_date('03-12-1981', 'dd-mm-yyyy'), 'ANALYST', 3000, 20, 7566);
 
insert into Employee (EMP_ID, EMP_NAME, EMP_NO, HIRE_DATE, JOB, SALARY, DEPT_ID, MNG_ID)
values (7369, 'SMITH', 'E7369', to_date('17-12-1980', 'dd-mm-yyyy'), 'CLERK', 800, 20, 7902);
 
insert into Employee (EMP_ID, EMP_NAME, EMP_NO, HIRE_DATE, JOB, SALARY, DEPT_ID, MNG_ID)
values (7698, 'BLAKE', 'E7698', to_date('01-05-1981', 'dd-mm-yyyy'), 'MANAGER', 2850, 30, 7839);
 
insert into Employee (EMP_ID, EMP_NAME, EMP_NO, HIRE_DATE, JOB, SALARY, DEPT_ID, MNG_ID)
values (7499, 'ALLEN', 'E7499', to_date('20-02-1981', 'dd-mm-yyyy'), 'SALESMAN', 1600, 30, 7698);
 
insert into Employee (EMP_ID, EMP_NAME, EMP_NO, HIRE_DATE, JOB, SALARY, DEPT_ID, MNG_ID)
values (7521, 'WARD', 'E7521', to_date('22-02-1981', 'dd-mm-yyyy'), 'SALESMAN', 1250, 30, 7698);
 
insert into Employee (EMP_ID, EMP_NAME, EMP_NO, HIRE_DATE, JOB, SALARY, DEPT_ID, MNG_ID)
values (7654, 'MARTIN', 'E7654', to_date('28-09-1981', 'dd-mm-yyyy'), 'SALESMAN', 1250, 30, 7698);
 
insert into Employee (EMP_ID, EMP_NAME, EMP_NO, HIRE_DATE, JOB, SALARY, DEPT_ID, MNG_ID)
values (7782, 'CLARK', 'E7782', to_date('09-06-1981', 'dd-mm-yyyy'), 'MANAGER', 2450, 30, 7839);
 
insert into Employee (EMP_ID, EMP_NAME, EMP_NO, HIRE_DATE, JOB, SALARY, DEPT_ID, MNG_ID)
values (7788, 'SCOTT', 'E7788', to_date('19-04-1987', 'dd-mm-yyyy'), 'ANALYST', 3000, 20, 7566);
 
insert into Employee (EMP_ID, EMP_NAME, EMP_NO, HIRE_DATE, JOB, SALARY, DEPT_ID, MNG_ID)
values (7844, 'TURNER', 'E7844', to_date('08-09-1981', 'dd-mm-yyyy'), 'SALESMAN', 1500, 30, 7698);
 
insert into Employee (EMP_ID, EMP_NAME, EMP_NO, HIRE_DATE, JOB, SALARY, DEPT_ID, MNG_ID)
values (7876, 'ADAMS', 'E7876', to_date('23-05-1987', 'dd-mm-yyyy'), 'CLERK', 1100, 20, 7698);
 
insert into Employee (EMP_ID, EMP_NAME, EMP_NO, HIRE_DATE, JOB, SALARY, DEPT_ID, MNG_ID)
values (7900, 'ADAMS', 'E7900', to_date('03-12-1981', 'dd-mm-yyyy'), 'CLERK', 950, 30, 7698);
 
insert into Employee (EMP_ID, EMP_NAME, EMP_NO, HIRE_DATE, JOB, SALARY, DEPT_ID, MNG_ID)
values (7934, 'MILLER', 'E7934', to_date('23-01-1982', 'dd-mm-yyyy'), 'CLERK', 1300, 10, 7698);

insert into Employee (EMP_ID, EMP_NAME, EMP_NO, HIRE_DATE, JOB, SALARY, DEPT_ID, MNG_ID)
values (1405, 'Minh Thuy', 'E1405', to_date('23-01-2002', 'dd-mm-yyyy'), 'CLERK', 1300.456, 10, 7698);

insert into Employee (EMP_ID, EMP_NAME, EMP_NO, HIRE_DATE, JOB, SALARY, DEPT_ID, MNG_ID)
values (2809, 'Vuong Hoang', 'E2809', to_date('28-09-2002', 'dd-mm-yyyy'), 'CLERK', 1300.456, 10, 7698);
 
-------------------------------------------------------------------------------------------------
 
create view emp [()]

insert into Salary_Grade (GRADE, HIGH_SALARY, LOW_SALARY)
values (1, 9999, 3001);

--Chuyển kết quả của việc chia thành loại DECIMAL bằng cách sử dụng CAST() hoặc :: 
select emp_name, round(salary::DECIMAL,2) AS salary, job
from employee
where image is null and job not ilike '_A_'
order by emp_name desc;

create view employee_view as
select emp_id, emp_name, dept_id
from employee;

select * from employee_view

CREATE OR REPLACE FUNCTION getEmployyeeCount()
RETURNS integer
AS
$$
BEGIN
    RETURN (SELECT count(*) FROM employee);
END
$$
LANGUAGE plpgsql;

SELECT getEmployyeeCount();

CREATE OR REPLACE FUNCTION getEmployeeCountByDept(_dept_id int)
RETURNS integer
AS
$$
BEGIN
    RETURN (SELECT count(*) FROM employee WHERE dept_id = _dept_id);
END
$$
LANGUAGE plpgsql;

select getEmployeeCountByDept(30);

SELECT 
dept_id ,SUM (salary) AS salary
FROM
employee
GROUP BY
dept_id
ORDER BY
SUM (salary) DESC;

SELECT DISTINCT
em
FROM
employee
WHERE
year>= 1950 
GROUP BY
director
HAVING
SUM(oscars)>2
ORDER BY
director;

select *
from employee
where image is not null;

select e.emp_name, e.emp_no, d.dept_name
from employee e
inner join department d
on e.dept_id = d.dept_id;

select e.emp_name, e.emp_no, d.dept_name
from employee e
left join department d
on e.dept_id = d.dept_id;

--distinct Dùng để xóa những bản ghi trùng nhau
select distinct job
from employee;

select *
from employee
where salary not between 1000 and 1500
limit 8 offset 3;

select * from department;
select * from employee;
select * from salary_grade;
select * from timekeeper;

--SELECT
--coalesce(min(salary/0.1),0) as min_salary_employee,
--coalesce(max(salary/0.1),0) as max_salary_employee,
----coalesce(round(avg(salary),3),0) as avg_salary_employee
--from employee;