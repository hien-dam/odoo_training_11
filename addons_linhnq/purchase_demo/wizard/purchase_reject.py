from odoo import api, fields, models, tools, _

from ..models.purchase_request import states


class PurchaseRequestLine(models.TransientModel):
    _name = "purchase.reject"
    _description = "Purchase Reject"

    approved_date = fields.Date(default=fields.Date.context_today)
    description = fields.Text()
    purchase_id = fields.Many2one('purchase.request', string='Purchase Request')

    def action_done(self):
        des = str(self.approved_date.strftime('%Y%m%d')) + ': ' + str(self.description)
        self.purchase_id.write({'description':  str(des), 'status': states[4][0]})


