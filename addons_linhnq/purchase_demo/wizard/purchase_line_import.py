from odoo import models, fields
import openpyxl
import base64
from io import BytesIO
from odoo.exceptions import ValidationError


class PurchaseLineImport(models.TransientModel):
    _name = "purchase.line.import"
    file = fields.Binary(string="File", required=True)

    def import_purchase_line(self):
        try:
            wb = openpyxl.load_workbook(
                filename=BytesIO(base64.b64decode(self.file)), read_only=True
            )
            ws = wb.active
            for record in ws.iter_rows(min_row=2, max_row=None, min_col=None, max_col=None, values_only=True):
                search = self.env['product.product'].browse(record[0])
                if search:
                    self.env['purchase.request.line'].create({
                        'product_id': search.id,
                        'request_quantity': record[1],
                        'delivered_quantity': record[6],
                    })
        except:
            raise ValidationError("The product not exit")
