from odoo import api, fields, models, tools, _
from odoo.exceptions import ValidationError


class PurchaseRequestLine(models.Model):
    _name = "purchase.request.line"
    _description = "Purchase Request Line"

    product_id = fields.Many2one('product.product')
    request_quantity = fields.Integer(string='Request quantity', required=True, default=0)
    estimated_unit_price = fields.Float(compute='_compute_unit_price', store=True)
    estimated_subtotal = fields.Float()
    due_date = fields.Date()
    description = fields.Text()
    delivered_quantity = fields.Integer()
    purchase_id = fields.Many2one('purchase.request', string='Purchase', ondelete='cascade')

    @api.depends('product_id')
    def _compute_unit_price(self):
        for purchase in self:
            for product in purchase.product_id:
                purchase.estimated_unit_price = product.list_price

    @api.constrains('request_quantity')
    def _check_product_quantity(self):
        for product in self:
            if product.request_quantity < 1:
                raise ValidationError("Amount must greater than 0 ")


