from dateutil.utils import today
from odoo import api, fields, models, tools, _
from odoo.exceptions import ValidationError

states = [('draft', 'Draft'), ('doing', 'Send'), ('done', 'Approved'), ('success', 'Success'), ('reject', 'Reject')]


class PurchaseRequest(models.Model):
    _name = "purchase.request"
    _description = "Purchase Request"

    name = fields.Text(string='Ticket Number', required=True, readonly=True, default=lambda self: _('New'))
    requested_by = fields.Many2one('res.users', required=True, default=lambda self: self.env.uid)
    department_id = fields.Many2one('hr.department', required=True, default=lambda self: self.env.user.department_id)
    cost_total = fields.Float(string='Cost total', compute='_compute_cost_total', store=True)
    creation_date = fields.Date(string='Creation Date', default=lambda self: today())
    due_date = fields.Date()
    approved_date = fields.Date()
    company_id = fields.Many2one('res.company', required=True, default=lambda self: self.env.user.company_id)
    purchase_line_id = fields.One2many('purchase.request.line', 'purchase_id', string='Purchase Lines')
    status = fields.Selection(states, default=states[0][0])
    description = fields.Text()

    @api.model
    def create(self, vals):
        if vals.get('name', _('New')) == _('New'):
            seq = self.env['ir.sequence'].next_by_code('purchase.ticket')
            if seq:
                vals['name'] = seq
        res = super(PurchaseRequest, self).create(vals)
        return res

    @api.constrains('purchase_line_id')
    def _check_product_exits(self):
        for record in self:
            exist_product_list = []
            for line in record.purchase_line_id:
                if line.product_id in exist_product_list:
                    raise ValidationError("The product with id: " + str(line.product_id['id']) + " is duplicate")
                exist_product_list.append(line.product_id)

    @api.depends('purchase_line_id')
    def _compute_cost_total(self):
        for purchase in self:
            cost_total = 0
            for item in purchase.purchase_line_id:
                cost_total += item.request_quantity * item.estimated_unit_price
            purchase.cost_total = cost_total

    def write(self, values):
        override_write = super(PurchaseRequest, self).write(values)
        return override_write

    def unlink(self):
        unlink = super(PurchaseRequest, self).unlink()
        return unlink

    def doing_confirm(self):
        for order in self:
            order.status = states[1][0]

    def done_confirm(self):
        for order in self:
            order.approved_date = today()
            order.status = states[2][0]

    def success_confirm(self):
        for order in self:
            order.status = states[3][0]

    def reject_confirm(self):
        return {
            'name': 'Request reject',
            'domain': [],
            'res_model': 'purchase.reject',
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'view_type': 'form',
            'view_id': self.env.ref('purchase_demo.purchase_reject_form_view').id,
            'context': {
                'default_purchase_id': self.id,
            },
            'target': 'new',
        }

    def import_purchase_line(self):
        return {
            'name': 'Import purchase line',
            'domain': [],
            'res_model': 'purchase.line.import',
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'view_type': 'form',
            'view_id': self.env.ref('purchase_demo.purchase_line_import_from_view').id,
            'context': {
                'default_purchase_id': self.id,
            },
            'target': 'new',
        }
