# -*- coding: utf-8 -*-
{
    'name': 'Purchase demo',
    'version': '1.0',
    'summary': 'App mua hang cho phep chap nhan hoac tu choi',
    'sequence': 10,
    'description': """""",
    'category': '',
    'website': '',
    'images': [],
    'depends': ['base', 'purchase'],
    'data': [
        'security/ir.model.access.csv',
        'views/purchase_views.xml',
        'wizard/purchase_reject_view.xml',
        'wizard/purchase_line_import_view.xml',
        'data/ticket_sequence.xml',
        'security/purchase_security.xml',
    ],
    'demo': [],
    'installable': True,
    'application': True,
    'auto_install': False,
    'post_init_hook': '',
    'assets': {},
    'license': '',
}