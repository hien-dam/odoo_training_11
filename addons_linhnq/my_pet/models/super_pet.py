from odoo import api, fields, models, tools, _


class SuperPet(models.Model):
    _name = "super.pet"  # <- new model name
    _inherit = "my.pet"  # <- inherit fields and methods from model "my.pet"
    _description = "Prototype inheritance"

    # add new field
    is_super_strength = fields.Boolean("Is Super Strength", default=False)
    is_fly = fields.Boolean("Is Super Strength", default=False)
    planet = fields.Char("Planet")
