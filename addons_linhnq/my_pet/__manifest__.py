# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': 'Pet',
    'version': '1.2',
    'summary': '',
    'sequence': 10,
    'description': """""",
    'category': 'Accounting/Accounting',
    'website': 'https://www.odoo.com/app/invoicing',
    'depends': ['base'],
    'data': [
        'security/ir.model.access.csv',
        'views/purchase_views.xml',
    ],

}
