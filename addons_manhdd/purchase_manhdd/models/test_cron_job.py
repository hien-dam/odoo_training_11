# -*- coding: utf-8 -*-

from odoo import api, fields, models
from datetime import datetime
import os


class TestCronJob(models.Model):
    _name = 'test.cron.job'

    @staticmethod
    def run():
        os.system("echo 'Test Cron: %s' >> /tmp/test.txt" % datetime.now())
