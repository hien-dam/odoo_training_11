# -*- coding: utf-8 -*-
from odoo import api, fields, models
from odoo.exceptions import ValidationError


class PurchaseRequestLine(models.Model):
    _name = "purchase.request.line"
    _description = "Purchase request line model"

    product = fields.Many2one('product.product', string='Product Id', required=True)
    request_quantity = fields.Integer(string='Request quantity', default=0)
    estimated_unit_price = fields.Float(string='Estimate Unit Price', compute='_compute_estimated_unit_price', store=True)
    estimated_subtotal = fields.Float(string='Estimate Subtotal', compute='_compute_estimated_subtotal', store=True)
    due_date = fields.Date(string='Due Date')
    description = fields.Text(string='Description')
    delivered_quantity = fields.Integer(string='Delivered Quantity', default=0)
    purchase_id = fields.Many2one('purchase.request', string='Purchase')

    @api.depends('product', )
    def _compute_estimated_unit_price(self):
        for rec in self:
            for each_product in rec.product:
                rec.estimated_unit_price = each_product.list_price

    @api.depends('estimated_unit_price', 'request_quantity')
    def _compute_estimated_subtotal(self):
        for rec in self:
            rec.estimated_subtotal = rec.estimated_unit_price * rec.request_quantity

    @api.constrains('request_quantity')
    def _check_product_exits(self):
        for record in self:
            if record.request_quantity < 1:
                raise ValidationError("Number or request should be greater than 0!")