# -*- coding: utf-8 -*-
from dateutil.utils import today

from odoo import api, fields, models, _
from odoo.exceptions import ValidationError

states = [('requesting', 'Draft'), ('waiting', 'Waiting'), ('approved', 'Approved'), ('finished', 'Finished'),
          ('rejected', 'Rejected')]


class PurchaseRequest(models.Model):
    _name = "purchase.request"
    _description = "Purchase request model"

    name = fields.Text(string='Ticket Number', required=True, readonly=True, default=_('New'))
    requested_by = fields.Many2one('res.users', required=True, default=lambda self: self.env.uid)
    department = fields.Many2one('hr.department', required=True, default=lambda self: self.env.user.department_id)
    cost_total = fields.Float(string='Cost total', compute='_compute_cost_total', store=True)
    creation_date = fields.Date(string='Creation Date', default=fields.Date.context_today)
    due_date = fields.Date(string='Due Date')
    approved_date = fields.Date(string='Approved Date', readonly=True)
    reject_reason = fields.Text(string='Reject Reason')
    reject_date = fields.Date(string='Reject Date')
    company = fields.Many2one('res.company', readonly=True, default=lambda self: self.env.user.company_id)
    purchase_line = fields.One2many('purchase.request.line', 'purchase_id', string='Purchase Lines')
    request_status = fields.Selection(states, string='Request Status', default='requesting')

    @api.model
    def create(self, vals):
        if vals.get('name', _('New')) == _('New'):
            sequence = self.env['ir.sequence'].next_by_code('purchase.ticket')
            if sequence:
                vals['name'] = sequence
        res = super(PurchaseRequest, self).create(vals)
        return res

    @api.constrains('purchase_line')
    def _check_product_exits(self):
        for record in self:
            exist_product_list = []
            for line in record.purchase_line:
                if line.product in exist_product_list:
                    raise ValidationError("The product with id: " + str(line.product['id']) + " is duplicate")
                exist_product_list.append(line.product)

    def write(self, vals):
        res = super(PurchaseRequest, self).write(vals)
        return res

    @api.onchange('due_date')
    def _set_due_date(self):
        self.purchase_line.write({'due_date': self.due_date})

    @api.depends('purchase_line')
    def _compute_cost_total(self):
        for rec in self:
            cost_total = 0
            for each_purchase_line in rec.purchase_line:
                cost_total += each_purchase_line.estimated_subtotal
            rec.cost_total = cost_total

    def requesting(self):
        for rec in self:
            rec.request_status = states[0][0]

    def waiting(self):
        for rec in self:
            rec.request_status = states[1][0]

    def approved(self):
        for rec in self:
            rec.request_status = states[2][0]
            rec.approved_date = today()

    def finished(self):
        for rec in self:
            rec.request_status = states[3][0]

    def rejected(self):
        return {
            'name': "Reject Form",
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'purchase.reject.wizard',
            'view_id': self.env.ref('purchase_manhdd.purchase_reject_reason').id,
            'target': 'new',
            'context': {
                'default_purchase_id': self.id
            }
        }
