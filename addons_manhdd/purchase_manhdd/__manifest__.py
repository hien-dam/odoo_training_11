# -*- coding: utf-8 -*-
{
    'name': 'app mua hang',
    'version': '1.0',
    'summary': 'App mua hang cho phep chap nhan hoac tu choi',
    'sequence': 10,
    'description': """
Payments
====================
Manh DD
    """,
    'category': '',
    'website': '',
    'images': [],
    'depends': ['base', 'purchase'],
    'data': [
        'security/purchase_security.xml',
        'security/ir.model.access.csv',
        'data/ticket_sequence.xml',
        'data/ir_cron_test_cron_job.xml',
        'views/purchase_view.xml',
        'wizard/reject_reason_view.xml',
    ],
    'demo': [],
    'installable': True,
    'application': True,
    'auto_install': False,
    'post_init_hook': '',
    'assets': {},
    'license': '',
}
