
from ..models.purchase_request import states
from odoo import fields, models


class PurchaseRejectWizard(models.TransientModel):
    _name = 'purchase.reject.wizard'
    _description = 'Reject reason'

    date = fields.Date(string='Date', default=fields.Date.context_today)
    description = fields.Text(string='Reason')
    purchase_id = fields.Many2one('purchase.request', string='Purchase Request Id')

    def action_update_reject_reason(self):
        self.purchase_id.write({'reject_reason': self.description,
                                'request_status': states[4][0],
                                'reject_date': self.date})
